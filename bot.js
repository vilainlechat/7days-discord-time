const { Client, Intents } = require('discord.js');
const client = new Client({
  intents: [ Intents.FLAGS.GUILDS ]
});

const config = require("./config.json");
const http = require("http");

let currentTick = 0;

const formatServerTime = (gametime) => {
  let serverTime = ""

  if (currentTick <= 1) {
    serverTime += `${config.human_days[gametime.days%7]} `
  } else {
    serverTime += `${config.day_abbreviation} ${gametime.days} `
  }

  if (gametime.hours < 10) {
    gametime.hours = `0${gametime.hours}`
  }
  if (gametime.minutes < 10) {
    gametime.minutes = `0${gametime.minutes}`
  }

  serverTime += `${gametime.hours}:${gametime.minutes}`

  if (currentTick >= 3) {
    currentTick = 0;
  } else {
    currentTick++;
  }

  return serverTime
}

client.on("ready", () => {
  console.log(`Successfully logged in as ${client.user.tag}!`);

  setInterval(() => {

    let getstats_url = `${config.webmap_url}/api/getstats`
    if (config.webmap_adminuser != "" && config.webmap_admintoken != "") {
      getstats_url += `?adminuser=${encodeURIComponent(config.webmap_adminuser)}&admintoken=${encodeURIComponent(config.webmap_admintoken)}`
    }

    http.get(getstats_url, res => {
      let data = "";
      res.on("data", (chunk) => { data += chunk; })
      res.on("end", () => {
        try {
          const serverStats = JSON.parse(data);
          let activityString = "";

          if ( (serverStats.gametime.days % 7) == 0 || ((serverStats.gametime.days % 7) == 1) && serverStats.gametime.hours < 4 ) {
            if (serverStats.gametime.days % 7 == 0 && serverStats.gametime.hours < 22) {
              activityString = `${config.blood_day_before_emoji} `
            } else {
              activityString = `${config.blood_day_emoji} `
            }
          } else {
            activityString = `${config.normal_day_emoji} `
          }

          activityString += formatServerTime(serverStats.gametime)
          activityString += ` - ${serverStats.players}/${config.server_max_players}`

          client.user.setActivity(activityString)
        } catch (e) {
          console.log(e);
          console.log(res.statusCode);
          console.log(res.statusMessage);
          client.user.setActivity(config.offline_server)
        }
      });

    }).on("error", (err) => {
      console.log(err);
      client.user.setActivity(config.offline_server)
    })

  }, 5000);
})

client.login(config.discord_token)
