# 7 Days To Die Discord bot time

A simple Discord bot that display your gameserver state in user activity.

![](./medias/normal-day1.png)

The current day alternate with the total days

![](./medias/normal-day2.png)

And the bloodmoon day it show a different emoji

![](./medias/blood-day1.png)

# Usage

### Prerequisite  
1. Having a machine with Node.JS => https://nodejs.org/en/
  * On Linux install the package following this documentation https://nodejs.org/en/download/package-manager/
  * On Windows just download the installer


2. You need to create a new Discord application (https://discord.com/developers/applications) and then a bot. Configure the name as you want.
Once the bot is created, under the "Bot" tab, click on the link "Click to Reveal Token" and copy the token.

3. Invite the bot on you server.
 * https://discord.com/api/oauth2/authorize?client_id=CLIENT_ID&permissions=0&scope=bot
 * In this URL, replace the CLIENT_ID with your APPLICATION ID (you can see it in General Information tab)

### Installation
1. Clone this repository and cd into

2. Rename the config example file `config.json.example.en` or `config.json.example.fr` to `config.json`

3. Open the `config.json` file in your text editor and configure the `webmap_url` and `discord_token` (the one you copied in step 2 of Prerequisite)


4. Open a terminal and run the following command inside the cloned repository
  * `npm install`
  * `node bot.js`
  * If the bot successfully show your gameserver status like on the screenshots above, you can follow the step 5

### Running the bot

* Linux
    * Run the bot via Forever ( it will run in background but will stop after system reboot. use stop instead of start to shutdown )
      * `$ sudo npm install forever -g`
      * `$ forever start bot.js`

    * Manage the bot via SystemD ( it will run even after system reboot and restart in case of failure)
      * Edit the file `tools/7days-discord-time.service`
      * Change the path of the bot ( `ExecStart=node <PATH_TO_BOT>/bot.js` )
      * Add the service to SystemD units
        * `$ sudo cp 7days-discord-time.service /etc/systemd/system/`
        * `$ sudo systemctl enable 7days-discord-time.service`
        * `$ sudo systemctl start 7days-discord-time.service`

### License

DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
        Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.
